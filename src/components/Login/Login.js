import React, { useState, useEffect } from 'react';
import Button from "./LoginButton/LoginButton";
import Form from "./LoginForm/LoginForm";
import { useHistory } from 'react-router-dom';



function AppLogin({state}) {

  const [show, setShow] = useState(false); 
  const [emailErr, setEmailErr] = useState(false);  

  let history = useHistory();
  const redirect = (URL) => {
    history.push(URL);
  }

  if(localStorage.email !== undefined){
    redirect('/main');
  }

  const emailCheck = (e) => {
      e.preventDefault();
      const input = document.getElementsByClassName('form__input')[0];
      const email = e.target.elements.email.value;

      if(email === ""){
        setEmailErr(true);
      } 

      input.oninput = () => {
        setEmailErr(false);
      }

      let re = /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/;
      if(re.test(String(email).toLowerCase())){
        state.email = email;
        localStorage.setItem('email', email);
        redirect('/main');
      }else{
        setEmailErr(true);
      }
  }

 useEffect((clientId = '807224892616-i0s584h624v9f35qm0ku4sca8scuei8k') => {
    // const _onInit = auth2 => {
    //   // console.log('init OK', auth2)
    // }
    // const _onError = err => {
    //   console.log('error', err)
    // }
    // window.gapi.load('auth2', function() {
    //   window.gapi.auth2
    //     .init({
    //       client_id: clientId,
    //     })
    //     .then(_onInit, _onError)
    // })
  },[]);

  const signIn = () => {
    // const auth2 = window.gapi.auth2.getAuthInstance()
    // auth2.signIn().then(googleUser => {
    
    //   const profile = googleUser.getBasicProfile()
    //   localStorage.setItem('name', profile.getGivenName())
    //   localStorage.setItem('email', profile.getEmail())

    //   if(profile.getEmail() !== null){
    //     redirect('/main');
    //   }
    // })
  }

  return(
    <div className="login">
    {
      show&&<Form
        show={show} 
        setShow={setShow} 
        emailCheck={emailCheck}
        signIn={signIn}
        emailErr={emailErr}
        />
    } 
      <Button show={show} setShow={setShow}/>
    </div>
  );
}

export default AppLogin;
