import React from "react";
import "./loginButton.css"

const Button = ({show, setShow}) => (
    <div className="btn__wrap">
        <button type="button" className="button" onClick={() => {setShow(!show)}} >
            Press to log in
        </button>
    </div>
)

export default Button;