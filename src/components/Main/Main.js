import React, { useState, useEffect } from "react";
import { useHistory } from 'react-router-dom';
import axios from "axios";
import Header from "./MainHeader/MainHeader";
import Body from "./MainBody/MainBody";
import Form from "./MainForm/MainForm";

const Main = ({state}) => {

  let history = useHistory();
  const redirect = (URL) => {
    history.push(URL);
  }

  const [show, setShow] = useState(false);
  const [edit, setEdit] = useState(false);
  const [formErr, setFormErr] = useState(false);
  const [items, setItems] = useState([]);
  const [title, setTitle] = useState("");
  const [descr, setDescr] = useState("");

  let name = localStorage.getItem('name');
  let email = localStorage.getItem('email');

  const userName = () =>{
    name = (name !== null) ? name : email;
  }
  userName();

  if(localStorage.email === undefined){
    redirect('/');
  }

  async function iputsCheck (e) {
    e.preventDefault();
    const title = e.target.elements.AppInputFs.value;
    const descr = e.target.elements.AppInputSd.value;

    if(title === "" || descr === ""){
      setFormErr(true);
    }else{
      setFormErr(false);

      localStorage.setItem('title', title);
      localStorage.setItem('descr', descr);
      
      const item = {
        title: localStorage.getItem('title'),
        description: localStorage.getItem('descr'),
        author: localStorage.getItem('email'),
      }; 

      const response = await axios.post('http://localhost:8000/items', item);
      let newArr = [...items, response.data];
      setItems(newArr);
      setShow(!show);
    }
  }

  function changingTitle(e){
    const title = e.target.value;
    setTitle(title);
  }

  function changingDescr(e){
    const descr = e.target.value;
    setDescr(descr);
  }

  async function editCheck (e) {
    e.preventDefault();
    const title = e.target.elements.AppInputFs.value;
    const descr = e.target.elements.AppInputSd.value;

    if(title === "" || descr === ""){
      setFormErr(true);
    }else{
      setFormErr(false);

      let id = localStorage.editId;
      const response = await axios.put(`http://localhost:8000/items/${id}`, {
        title: title,
        description: descr,
      });

        const item = response.data;
        let editItem = items.map((i) =>{
          if(i._id === item._id){
              i.title = item.title;
              i.description = item.description;
          }
          return(i);
        });
        setItems(editItem);
      }
      setEdit(false);
      setShow(!show);
  }

  useEffect((user = localStorage.getItem('email')) => {
    const response = axios.get(`http://localhost:8000/items/${user}`).then((response) => {
      setItems(response.data);
    });
      if(response !== 0){
        
      }

    // if(localStorage.name !== undefined){
    //   const _onInit = auth2 => {
    //     // console.log('init OK', auth2)
    //   }
    //   const _onError = err => {
    //     console.log('error', err)
    //   }
    //   window.gapi.load('auth2', function() {
    //     window.gapi.auth2
    //       .init({
    //         client_id:
    //           process.env.REACT_APP_GOOGLE_CLIENT_ID,
    //       })
    //       .then(_onInit, _onError)
    //   })
    // }
  },[]);

  const signOut = () => {
    // if(localStorage.name !== undefined){
    //   const auth2 = window.gapi.auth2.getAuthInstance()
    //   auth2.signOut().then(function() {
    //     console.log('User signed out.')
    //   })
    // }
    state.name = null;
    state.email = null;
    localStorage.clear();
    redirect('/');
  }

  return(
    <div className="app-main">
        <Header 
          signOut={signOut}
          name={name}
          email={email}
        />
        <Body 
          show={show}
          setShow={setShow}
          setTitle={setTitle}
          setDescr={setDescr}
          items={items}
          setItems={setItems}
          setEdit={setEdit}
        />
        {
          show?<Form 
            show={show}
            setShow={setShow}
            title={title}
            descr={descr}
            changingTitle={changingTitle}
            changingDescr={changingDescr}
            formErr={formErr}
            iputsCheck={iputsCheck}
            editCheck={editCheck}
            edit={edit}
          />:null
        }
    </div>
  );
}

export default Main;