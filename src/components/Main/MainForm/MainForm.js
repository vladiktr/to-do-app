import React from "react";
import "./mainForm.css"

const MainForm = ({ iputsCheck, editCheck, setShow, show, formErr, edit, title, descr, changingTitle, changingDescr }) => (

    <div>
        <div className="AppForm__wrap form__wrap" onClick={() => {setShow(!show)}}></div>
        <form className="AppForm form" id="AppForm" onSubmit={edit? editCheck : iputsCheck}>
            <h3>
                {edit? "Edit item" : "Add item"}
            </h3>
            <p className="Appform-title">
                Title:
            </p>
            <input className="AppForm__input form__input AppInputFs" type="text" name="AppInputFs" value={title} onChange={changingTitle}></input>
            <p className="Appform-title">
                Description: 
            </p>
            <input className="AppForm__input form__input" type="text" name="AppInputSd" value={descr} onChange={changingDescr}></input>
            {formErr&&
                <div className="AppForm__result form__result">Fill all Fields</div>
            }
            <div className="AppForm__buttons form__buttons">
                <button type="submit" className="form__add-btn form__btn">
                    Add
                </button>
                <button type="button" className="form__close-btn form__btn" onClick={() => {setShow(!show)}}>
                    Close
                </button>
            </div>
        </form>
    </div>
)

export default MainForm;