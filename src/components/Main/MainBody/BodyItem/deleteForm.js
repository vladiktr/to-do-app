import React from "react";
import axios from "axios";
import "./deleteForm.css";

const DeleteForm = ({ setShowDel, showDel, item, items, setItems }) => {

     async function deleteItem() {
        const id = item._id;
        const result = await axios.delete(`http://localhost:8000/items/${id}`);
        let newArr = Array.prototype.filter.call(items, i => i._id !== result.data);
        setItems(newArr);
    }
    
    return(
        <div>
            <div className="AppForm__wrap form__wrap" onClick={() => {setShowDel(!showDel)}}></div>
            <form className="form" >
                <div className="form__title">Delete item</div>
                <div className="form__descr">Are you sure?</div>
                <div className="form__buttons">
                    <button type="button" className="form__add-btn form__btn" onClick={ () => { deleteItem(); setShowDel(!showDel); }}>Confirm</button>
                    <button type="button" className="form__close-btn form__btn" onClick={() => {setShowDel(!showDel)}}>Close</button>
                </div>
            </form>
        </div>
    )
}

export default DeleteForm;