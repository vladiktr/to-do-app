import React from "react";
import Item from "./BodyItem/MainBodyItem";

const Body = ({ items, show, setShow, setEdit, setItems, setTitle, setDescr }) => {
    return(
        <div className="app__body">
            <div className="app__body-container">
                <button className="item add-item" onClick={() => {setShow(!show); setTitle(""); setDescr("");}}>
                    <img src="/img/plus.png" alt=""></img>
                </button>
                {Array.prototype.map.call(items, (item, index) => {
                    return (
                        <div className="item" key={index}>
                            {<Item 
                                item={item} 
                                items={items}
                                show={show} 
                                setShow={setShow} 
                                setEdit={setEdit}
                                setItems={setItems}
                                setTitle={setTitle}
                                setDescr={setDescr}
                            />}
                        </div>
                    )
                })}
            </div>
        </div>
    )
} 

export default Body;