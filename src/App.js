import React from "react";
import Login from "./components/Login/Login";
import Main from "./components/Main/Main";
import { BrowserRouter as Router, Switch, Route} from "react-router-dom";

class App extends React.Component{

  constructor(props) {
    super(props);
    this.state = {email: null};
    this.state = {name: null};
  }

  render() {
    return(
        <div className="main">
          <Router>
            <Switch>
              <Route render = { props => <Login state={this.state} /> } exact path="/" />
              <Route render = { props => <Main state={this.state} /> } exact path="/main"/>
            </Switch>
          </Router>
        </div>
    );
  }
}

export default App;
